IMPORTANT NOTES FOR REVIEWER
============================

1. This project fully implements the coding kata by displaying transactions grouped into their respective statement dates and displayed in the table view. Trasaction dates are displayed in UK format i.e: dd/mm/yyyy
2. iOS 9.0 onwards supported. Tested on iPhone 5 and above iOS simulators in both orientations. 
3. The project has been developed on latest xcode 8.3.3 with Swift 3.1 without any warnings or errors.
4. The app is following MVVM (Model-View-ViewModel) architecture pattern. 
5. Test first development approach has been adopted and only most suitable unit tests have been written which covers all of the core application components.
6. I preferred to use all the built-in native iOS technologies and not using third-party libraries or CocoaPods for this small project.
7. This project is built on best software engineering practices. To name a few: SOLID principles, composition over inheritance, protocol oriented programming, loosely coupled architecture and TDD.

Thanks for your time.
