//
//  Result.swift
//
//  Created by Waheed Malik on 07/06/2017.
//  Copyright © 2017 Waheed Malik. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(Error)
}
