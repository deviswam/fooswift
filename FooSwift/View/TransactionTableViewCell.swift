//
//  TransactionTableViewCell.swift
//  FooSwift
//
//  Created by Waheed Malik on 08/06/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import UIKit

protocol TransactionTableViewCell {
    func configCell(with transaction: TransactionViewModel)
}

class FSTransactionTableViewCell: UITableViewCell, TransactionTableViewCell {
    func configCell(with transaction: TransactionViewModel) {
        self.textLabel?.text = transaction.date
        self.detailTextLabel?.text = transaction.amount
    }
}
