//
//  NSDate.swift
//  FooSwift
//
//  Created by Waheed Malik on 08/06/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import Foundation

extension Date {
    func formattedDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: self)
    }
}
