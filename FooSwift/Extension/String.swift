//
//  NSDate.swift
//  FooSwift
//
//  Created by Waheed Malik on 07/06/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import Foundation

extension String {
    func date() -> Date? {
        var calendar = Calendar.current
        let timezone = TimeZone(identifier: "UTC")!
        calendar.timeZone = timezone
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone =  timezone
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        guard let date = dateFormatter.date(from: self) else {
            return nil
        }
        
        let components = (calendar as NSCalendar).components([.year, .month, .day], from: date)
        return calendar.date(from: components)
    }
}
