//
//  AppDelegate.swift
//  FooSwift
//
//  Created by Tim on 24/02/2016.
//  Copyright © 2016 Tesco Bank. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // inject all the dependencies of MVVM module.
        if let transactionListVC = window?.rootViewController as? TransactionListViewController {
            let dataStore = LocalJSONDataStore()
            let accountManager = FSAccountManager(dataStore: dataStore)
            let transactionsTableViewDataSource = TransactionsTableViewDataSource()
            let transactionListViewModel = FSTransactionListViewModel(accountManager: accountManager, viewDelegate: transactionListVC)
            
            transactionListVC.viewModel = transactionListViewModel
            transactionListVC.transactionsTableViewDataSource = transactionsTableViewDataSource
            transactionsTableViewDataSource.viewModel = transactionListViewModel
        }
        return true
    }
}

