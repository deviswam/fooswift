//
//  TransactionListViewController.swift
//  FooSwift
//
//  Created by Waheed Malik on 08/06/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import UIKit

class TransactionListViewController: UIViewController {

    @IBOutlet weak var transactionsTableView: UITableView!
    var transactionsTableViewDataSource: TransactionsTableViewDataSource?
    var viewModel: TransactionListViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        transactionsTableView.dataSource = transactionsTableViewDataSource
        
        //ask for statements.
        viewModel?.getAccountStatements()
    }
}

extension TransactionListViewController: TransactionListViewModelViewDelegate  {
    func statementsLoaded(with result: Result<Void>) {
        switch result {
        case .success:
            self.transactionsTableView.reloadData()
        case .failure:
            print("Info (Inform user): Unable to fetch statements list")
        }
    }
}
