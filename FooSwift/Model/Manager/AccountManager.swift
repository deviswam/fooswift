//
//  AccountManager.swift
//  FooSwift
//
//  Created by Waheed Malik on 07/06/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import Foundation

enum AccountManagerError : Error {
    case noStatementsFound
    case noTransactionsFound
    
    func userFriendlyMessage() -> String {
        switch self {
        case .noStatementsFound:
            return "No Statements Found"
        case .noTransactionsFound:
            return "No Transactions Found"
        }
    }
}

protocol AccountManager {
    func loadAccountStatements(completionHandler: @escaping (_ result: Result<[Statement]>) -> Void)
}

class FSAccountManager: AccountManager {
    // MARK: PRIVATE VARIABLES
    private let dataStore: DataStore
    
    // MARK: INITIALIZER
    init(dataStore: DataStore) {
        self.dataStore = dataStore
    }
    
    // MARK: PUBLIC METHODS
    func loadAccountStatements(completionHandler: @escaping (_ result: Result<[Statement]>) -> Void) {
        var mResult: Result<[Statement]>!
        dataStore.fetchStatements { [weak self] (resultStatements: Result<[Statement]>) in
            switch resultStatements {
            case .success(let statements):
                self?.dataStore.fetchTransactions(completionHandler: { (resultTransactions: Result<[Transaction]>) in
                    switch resultTransactions {
                    case .success(let transactions):
                        mResult = .success(self!.group(transactions: transactions, with: statements))
                    case .failure(_):
                        mResult = .failure(AccountManagerError.noTransactionsFound)
                    }
                    completionHandler(mResult)
                })
            case .failure(_):
                mResult = .failure(AccountManagerError.noStatementsFound)
                completionHandler(mResult)
            }
        }
    }
    
    // MARK: PRIVATE HELPER METHODS
    private func group(transactions: [Transaction], with statements: [Statement]) -> [Statement] {
        //sort transactions in decending order
        let sortedTransactions = self.sort(transactions: transactions)
        
        //sort statements in decending order
        var sortedStatements = self.sort(statements: statements)
        
        // relate transactions to statements
        self.relate(transactions: sortedTransactions, with: &sortedStatements)
        
        //remove the statements which has no transactions
        sortedStatements = sortedStatements.filter{ return $0.transactions.count > 0}
        
        return sortedStatements
    }
    
    private func sort(transactions: [Transaction]) -> [Transaction] {
        let sortedTransactions = transactions.sorted { (lTransaction: Transaction, rTransaction: Transaction) -> Bool in
            return lTransaction.date > rTransaction.date
        }
        return sortedTransactions
    }
    
    private func sort(statements: [Statement]) -> [Statement] {
        let sortedStatements = statements.sorted { (lStatement: Statement, rStatement: Statement) -> Bool in
            return lStatement.date! > rStatement.date!
        }
        return sortedStatements
    }
    
    private func relate(transactions: [Transaction], with statements: inout [Statement]) {
        let recentStatement = FSStatement(date: nil)
        for transaction in transactions {
            // check with latest statement date.
            if transaction.date > statements.first!.date! {
                recentStatement.transactions.append(transaction)
                continue
            } else {
                for (i, var statement) in statements.enumerated() {
                    if transaction.date <= statement.date! {
                        if i < statements.count - 1 {
                            if transaction.date > statements[i+1].date! {
                                statement.transactions.append(transaction)
                                break
                            }
                        } else {
                            statement.transactions.append(transaction)
                            break
                        }
                    }
                }
            }
        }
        
        //append recent and oldest statements, if required.
        if recentStatement.transactions.count > 0 {
            statements.insert(recentStatement, at: 0)
        }
    }
}
