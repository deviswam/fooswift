//
//  Statement.swift
//  FooSwift
//
//  Created by Waheed Malik on 07/06/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import Foundation

protocol Statement {
    var date : Date? { get } // nil only for latest statement, has value for normal statements.
    var transactions: [Transaction] { get set }
}

class FSStatement: Statement {
    var date: Date?
    var transactions: [Transaction]
    
    init(date: Date?) {
        self.date = date
        transactions = [Transaction]()
    }
    
    convenience init?(strDate: String) {
        guard let iDate = strDate.date() else {
            return nil
        }
        self.init(date: iDate)
    }
}
