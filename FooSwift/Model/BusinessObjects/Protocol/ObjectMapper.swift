//
//  ObjectMapper.swift
//  WMWeatherAlert
//
//  Created by Waheed Malik on 07/06/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import Foundation

protocol ObjectMapper {
    init?(dictionary:Dictionary<String,Any>)
}
