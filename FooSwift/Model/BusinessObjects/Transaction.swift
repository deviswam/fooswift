//
//  Transaction.swift
//  FooSwift
//
//  Created by Waheed Malik on 07/06/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import Foundation

protocol Transaction {
    var date : Date { get }
    var amount : NSDecimalNumber { get }
}

class FSTransaction: Transaction, ObjectMapper {
    var date: Date
    var amount: NSDecimalNumber
    
    init(date: Date, amount: NSDecimalNumber) {
        self.date = date
        self.amount = amount
    }
    
    required convenience init?(dictionary: Dictionary<String, Any>) {
        guard let strDate = dictionary["occurred"] as? String, let iDate = strDate.date(),
              let dAmount = dictionary["amount"] as? Double else {
                return nil
        }

        let iAmount = NSDecimalNumber(value: dAmount as Double)
        
        self.init(date: iDate as Date, amount: iAmount)
    }
}
