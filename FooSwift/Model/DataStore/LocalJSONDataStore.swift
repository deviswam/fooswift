//
//  LocalJSONDataStore.swift
//  FooSwift
//
//  Created by Waheed Malik on 07/06/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import Foundation

enum LocalJSONDataStoreFiles : String {
    case statementDates = "statementdates"
    case transactions = "transactions"
}

enum DataStoreError: Error {
    case fileNotFound
    case invalidData
    case dataSerialization
}

protocol DataStore {
    func fetchStatements(completionHandler: @escaping (_ result: Result<[Statement]>) -> Void)
    func fetchTransactions(completionHandler: @escaping (_ result: Result<[Transaction]>) -> Void)
}

class LocalJSONDataStore: DataStore {
    // MARK: PRIVATE VARIABLES
    private let nsBundle: Bundle
    
    // MARK: INITIALIZER
    init(nsBundle: Bundle = Bundle.main) {
        self.nsBundle = nsBundle
    }
    
    // MARK: PUBLIC METHODS
    func fetchStatements(completionHandler: @escaping (_ result: Result<[Statement]>) -> Void) {
        guard let filePath = nsBundle.path(forResource: LocalJSONDataStoreFiles.statementDates.rawValue, ofType: "json") else {
            return completionHandler(.failure(DataStoreError.fileNotFound))
        }
        
        let concurrentQueue = DispatchQueue(label: "conQueue", attributes: .concurrent)
        concurrentQueue.async { [weak self] in
            self?.loadStatementDatesFile(from: filePath) { (result: Result<[Statement]>) in
                DispatchQueue.main.async {
                    completionHandler(result)
                }
            }
        }
    }
    
    func fetchTransactions(completionHandler: @escaping (_ result: Result<[Transaction]>) -> Void) {
        guard let filePath = nsBundle.path(forResource: LocalJSONDataStoreFiles.transactions.rawValue, ofType: "json") else {
            return completionHandler(.failure(DataStoreError.fileNotFound))
        }
        
        let concurrentQueue = DispatchQueue(label: "conQueue", attributes: .concurrent)
        concurrentQueue.async {  [weak self] in
            self?.loadTransactionFile(from: filePath) { (result: Result<[Transaction]>) in
                DispatchQueue.main.async {
                    completionHandler(result)
                }
            }
        }
    }
    
    // MARK: PRIVATE HELPER METHODS
    private func loadStatementDatesFile(from path: String, completionHandler: (Result<[Statement]>) -> Void) {
        var result: Result<[Statement]>!
        if let jsonData = try? Data(contentsOf: URL(fileURLWithPath: path))
        {
            do {
                if let statementDates = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String] {
                    result = .success(self.createStatements(from: statementDates))
                }
            } catch {
                result = .failure(DataStoreError.dataSerialization)
            }
        } else {
            result = .failure(DataStoreError.invalidData)
        }
        
        completionHandler(result)
    }
    
    private func createStatements(from statementDatesArray:[String]) -> [Statement] {
        let statements = statementDatesArray.map { (statementDate: String) -> Statement? in
            return FSStatement(strDate: statementDate)
            }.filter { (statement: Statement?) -> Bool in
                return statement != nil
            }.map {$0! as Statement}
        return statements
    }
    
    
    private func loadTransactionFile(from path: String, completionHandler: (Result<[Transaction]>) -> Void) {
        var result: Result<[Transaction]>!
        if let jsonData = try? Data(contentsOf: URL(fileURLWithPath: path))
        {
            do {
                if let transactionsArray = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [[String: AnyObject]] {
                    result = .success(self.createTransactions(from: transactionsArray))
                }
            } catch {
                result = .failure(DataStoreError.dataSerialization)
            }
        } else {
            result = .failure(DataStoreError.invalidData)
        }
        
        completionHandler(result)
    }
    
    private func createTransactions(from transactionsArray:[[String: AnyObject]]) -> [Transaction] {
        let transactions = transactionsArray.map { (transactionDictionary: [String: AnyObject]) -> Transaction? in
            return FSTransaction(dictionary: transactionDictionary)
            }.filter { (transaction: Transaction?) -> Bool in
                return transaction != nil
            }.map {$0! as Transaction}
        return transactions
    }
}
