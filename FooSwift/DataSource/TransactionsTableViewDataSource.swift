//
//  TransactionsTableViewDataSource.swift
//  FooSwift
//
//  Created by Waheed Malik on 08/06/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import UIKit

class TransactionsTableViewDataSource : NSObject, UITableViewDataSource {
    var viewModel: TransactionListViewModel?
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let viewModel = viewModel else { return  nil }
        return viewModel.titleOfStatement(at: section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let viewModel = viewModel else { return  0 }
        return viewModel.numberOfStatements()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = viewModel else { return 0 }
        return viewModel.numberOfTransactions(at: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTableViewCell", for: indexPath)
        if let transactionCell = cell as? TransactionTableViewCell,
            let transaction = viewModel?.transaction(at: indexPath.row, statementIndex: indexPath.section) {
            transactionCell.configCell(with: transaction)
        }
        return cell
    }
}
