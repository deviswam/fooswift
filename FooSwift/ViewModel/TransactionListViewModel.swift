//
//  TransactionListViewModel.swift
//  FooSwift
//
//  Created by Waheed Malik on 07/06/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import Foundation

// MARK:- VIEW DELEGATE PROTOCOL
protocol TransactionListViewModelViewDelegate: class {
    func statementsLoaded(with result: Result<Void>)
}

// MARK:- VIEWMODEL PROTOCOL
protocol TransactionListViewModel {
    func getAccountStatements()
    func titleOfStatement(at statementIndex: Int) -> String
    func numberOfStatements() -> Int
    func numberOfTransactions(at statementIndex: Int) -> Int
    func transaction(at transactionIndex: Int, statementIndex: Int) -> TransactionViewModel?
}

class FSTransactionListViewModel: TransactionListViewModel {
    // MARK: PRIVATE VARIABLES
    private let accountManager: AccountManager
    private weak var viewDelegate: TransactionListViewModelViewDelegate!
    private var statements: [Statement]?
    
    // MARK: INITIALIZER
    init(accountManager: AccountManager, viewDelegate: TransactionListViewModelViewDelegate) {
        self.accountManager = accountManager
        self.viewDelegate = viewDelegate
    }
    
    // MARK: PUBLIC METHODS
    func getAccountStatements() {
        var vmResult: Result<Void>!
        accountManager.loadAccountStatements {[weak self] (result: Result<[Statement]>) in
            switch result {
            case .success(let statements):
                self?.statements = statements
                vmResult = .success()
            case .failure(let error):
                vmResult = .failure(error)
            }
            self?.viewDelegate.statementsLoaded(with: vmResult)
        }
    }
    
    func titleOfStatement(at statementIndex: Int) -> String {
        guard let statements = statements, statementIndex < statements.count,
        let date = statements[statementIndex].date else { return "" }
        return date.formattedDate()
    }
    
    func numberOfStatements() -> Int {
        guard let statements = statements else { return 0 }
        return statements.count
    }
    
    func numberOfTransactions(at statementIndex: Int) -> Int {
        guard let statements = statements, statementIndex < statements.count else { return 0 }
        let transactions = statements[statementIndex].transactions
        return transactions.count
    }
    
    func transaction(at transactionIndex: Int, statementIndex: Int) -> TransactionViewModel? {
        guard let statements = statements, statementIndex < statements.count else { return nil }
        let transactions = statements[statementIndex].transactions
        if transactionIndex < transactions.count {
            let transaction = transactions[transactionIndex]
            return FSTransactionViewModel(transaction: transaction)
        }
        return nil
    }
}
