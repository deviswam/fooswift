//
//  TransactionViewModel.swift
//  FooSwift
//
//  Created by Waheed Malik on 07/06/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import Foundation

protocol TransactionViewModel {
    var date: String { get }
    var amount: String { get }
}

class FSTransactionViewModel: TransactionViewModel {
    var date: String = ""
    var amount: String = ""
    init(transaction: Transaction) {
        self.date = transaction.date.formattedDate()
        self.amount = "\(transaction.amount)"
    }
}
