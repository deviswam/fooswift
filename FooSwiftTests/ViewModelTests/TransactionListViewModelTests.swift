//
//  TransactionListViewModel.swift
//  FooSwift
//
//  Created by Waheed Malik on 07/06/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import XCTest
@testable import FooSwift

extension TransactionListViewModelTests {
    class MockAccountManager: AccountManager {
        var loadAccountStatements = false
        var completionHandler: ((Result<[Statement]>) -> Void)?
        func loadAccountStatements(completionHandler: @escaping (_ result: Result<[Statement]>) -> Void) {
            loadAccountStatements = true
            self.completionHandler = completionHandler
        }
    }
    
    class MockTransactionListViewModelViewDelegate: TransactionListViewModelViewDelegate {
        var expectation: XCTestExpectation?
        var transactionListViewModelResult: Result<Void>?
        
        func statementsLoaded(with result: Result<Void>) {
            guard let asynExpectation = expectation else {
                XCTFail("MockTransactionListViewModelViewDelegate was not setup correctly. Missing XCTExpectation reference")
                return
            }
            self.transactionListViewModelResult = result
            asynExpectation.fulfill()
        }
    }
}

class TransactionListViewModelTests: XCTestCase {
    var sut: FSTransactionListViewModel!
    var mockAccountManager: MockAccountManager!
    var mockViewDelegate: MockTransactionListViewModelViewDelegate!
    
    override func setUp() {
        super.setUp()
        mockAccountManager = MockAccountManager()
        mockViewDelegate = MockTransactionListViewModelViewDelegate()
        sut = FSTransactionListViewModel(accountManager: mockAccountManager, viewDelegate: mockViewDelegate)
    }
    
    func testConformanceToTransactionListViewModelProtocol() {
        // Assert
        XCTAssertTrue((sut as AnyObject) is TransactionListViewModel, "FSTransactionListViewModel should conforms to TransactionListViewModel protocol")
    }
    
    func testGetAccountStatements_shouldAskAccoutManagerForListOfStatements() {
        // Act
        sut.getAccountStatements()
        // Assert
        XCTAssert(mockAccountManager.loadAccountStatements)
    }
    
    func testGetAccountStatements_whenStatementsFound_raisesStatementsLoadedDelegateCallbackWithSuccess() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getAccountStatements()
        mockAccountManager.completionHandler?(Result.success([FSStatement(date: Date())]))
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            
            switch self.mockViewDelegate.transactionListViewModelResult! {
            case .success():
                XCTAssert(true)
            default:
                XCTFail("View delegate should only receive success result when statements found")
            }
        }
    }
    
    func testGetAccountStatements_whenNoStatementsFound_raisesStatementsLoadedDelegateCallbackWithFailure() {
        // Arrange
        let asynExpectation = expectation(description: "asyncFunction")
        mockViewDelegate.expectation = asynExpectation
        
        // Act
        sut.getAccountStatements()
        mockAccountManager.completionHandler?(Result.failure(AccountManagerError.noStatementsFound))
        
        waitForExpectations(timeout: 1.0) {[unowned self] error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            
            switch self.mockViewDelegate.transactionListViewModelResult! {
            case .success():
                XCTFail("View delegate should receive failure with an error when NO statements found")
            default:
                XCTAssert(true)
            }
        }
    }
}
