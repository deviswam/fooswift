//
//  AccountManagerTests.swift
//  FooSwift
//
//  Created by Waheed Malik on 08/06/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import XCTest
@testable import FooSwift

extension AccountManagerTests {
    class MockDataStore: DataStore {
        var fetchStatementsCalled = false
        var fetchStatementsCompletionHandler: ((_ result: Result<[Statement]>) -> Void)?
        func fetchStatements(completionHandler: @escaping (_ result: Result<[Statement]>) -> Void) {
            fetchStatementsCalled = true
            self.fetchStatementsCompletionHandler = completionHandler
        }
        
        var fetchTransactionsCalled = false
        var fetchTransactionsCompletionHandler: ((_ result: Result<[Transaction]>) -> Void)?
        func fetchTransactions(completionHandler: @escaping (_ result: Result<[Transaction]>) -> Void) {
            fetchTransactionsCalled = true
            self.fetchTransactionsCompletionHandler = completionHandler
        }
    }
}

class AccountManagerTests: XCTestCase {
    
    var sut: FSAccountManager!
    var mockDataStore: MockDataStore!
    
    override func setUp() {
        super.setUp()
        mockDataStore = MockDataStore()
        sut = FSAccountManager(dataStore: mockDataStore)
    }
    
    func testConformanceToAccountManagerProtocol() {
        // Assert
        XCTAssertTrue((sut as AnyObject) is AccountManager, "FSAccountManager should conforms to AccountManager protocol")
    }
    
    func testLoadAccountStatements_asksDataStoreToFetchStatements() {
        // Act
        sut.loadAccountStatements { (result: Result<[Statement]>) in
            
        }
        
        // Assert
        XCTAssert(mockDataStore.fetchStatementsCalled)
    }
    
    func testLoadAccountStatements_whenStatementsFound_asksDataStoreToFetchTransactions() {
        // Act
        sut.loadAccountStatements { (result: Result<[Statement]>) in
            
        }
        
        mockDataStore.fetchStatementsCompletionHandler?(.success([FSStatement(date: nil)]))
        
        // Assert
        XCTAssert(mockDataStore.fetchTransactionsCalled)
    }
    
    func testLoadAccountStatements_whenTransactionDateIsPriorToStatementDate_shouldAssociateTransactionWithStatement() {
        // Arrange
        var receivedStatements : [Statement]?
        var receivedError : Error?
        let expectedStatements = [FSStatement(strDate: "2014-06-16")!, FSStatement(strDate: "2014-04-16")!]
        let expectedTransactions = [FSTransaction(date: "2014-05-16".date()!, amount: -100)]
        
        // Act
        sut.loadAccountStatements { (result: Result<[Statement]>) in
            switch result {
            case .success(let statements):
                receivedStatements = statements
            case .failure(let error):
                receivedError = error
            }
        }
        
        mockDataStore.fetchStatementsCompletionHandler?(.success(expectedStatements))
        mockDataStore.fetchTransactionsCompletionHandler?(.success(expectedTransactions))
        
        // Assert
        XCTAssertNil(receivedError)
        XCTAssertNotNil(receivedStatements)
        XCTAssertNotNil(receivedStatements?.first?.date) // normal statements have a date.
        XCTAssert(receivedStatements?.first?.transactions.count == 1)
    }
    
    func testLoadAccountStatements_whenTransactionDateIsAfterMostRecentStatementDate_shouldAssociateTransactionWithLatestStatement() {
        // Arrange
        var receivedStatements : [Statement]?
        var receivedError : Error?
        let expectedStatements = [FSStatement(strDate: "2014-06-16")!, FSStatement(strDate: "2014-04-16")!]
        let expectedTransactions = [FSTransaction(date: "2014-06-20".date()!, amount: -100)]
        
        // Act
        sut.loadAccountStatements { (result: Result<[Statement]>) in
            switch result {
            case .success(let statements):
                receivedStatements = statements
            case .failure(let error):
                receivedError = error
            }
        }
        
        mockDataStore.fetchStatementsCompletionHandler?(.success(expectedStatements))
        mockDataStore.fetchTransactionsCompletionHandler?(.success(expectedTransactions))
        
        // Assert
        XCTAssertNil(receivedError)
        XCTAssertNotNil(receivedStatements)
        XCTAssertNil(receivedStatements?.first?.date)  //Latest statement has nil date
        XCTAssert(receivedStatements?.first?.transactions.count == 1)
    }
    
    func testLoadAccountStatements_whenTransactionDateIsBeforeOldestStatementDate_shouldAssociateTransactionWithOldestStatement() {
        // Arrange
        var receivedStatements : [Statement]?
        var receivedError : Error?
        let expectedStatements = [FSStatement(strDate: "2014-06-16")!, FSStatement(strDate: "2014-04-16")!]
        let expectedTransactions = [FSTransaction(date: "2014-03-20".date()!, amount: -100)]
        
        // Act
        sut.loadAccountStatements { (result: Result<[Statement]>) in
            switch result {
            case .success(let statements):
                receivedStatements = statements
            case .failure(let error):
                receivedError = error
            }
        }
        
        mockDataStore.fetchStatementsCompletionHandler?(.success(expectedStatements))
        mockDataStore.fetchTransactionsCompletionHandler?(.success(expectedTransactions))
        
        // Assert
        XCTAssertNil(receivedError)
        XCTAssertNotNil(receivedStatements)
        XCTAssertNotNil(receivedStatements?.first?.date)  //Oldest statement has a date
        XCTAssert(receivedStatements?.last?.transactions.count == 1)
    }
}
