//
//  LocalJSONDataStoreTests.swift
//  FooSwift
//
//  Created by Waheed Malik on 07/06/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import XCTest
@testable import FooSwift

extension LocalJSONDataStoreTests {
    class MockNSBundle: Bundle {
        var pathForResourceCalledWithFileName: String?
        var filePath: String?
        override func path(forResource name: String?, ofType ext: String?) -> String? {
            self.pathForResourceCalledWithFileName = name
            return Bundle.main.path(forResource: name, ofType: ext)
        }
    }
}

class LocalJSONDataStoreTests: XCTestCase {
    var sut: LocalJSONDataStore!
    var mockNSBundle: MockNSBundle!
    
    override func setUp() {
        super.setUp()
        mockNSBundle = MockNSBundle()
        sut = LocalJSONDataStore(nsBundle: mockNSBundle)
    }
    
    func testConformanceToDataStore() {
        XCTAssertTrue((sut as AnyObject) is DataStore)
    }
    
    func testFetchStatements_shouldAskMainBundleToLookForStatementsJSONFile() {
        // Act
        sut.fetchStatements { (result) in
            
        }
        
        // Assert
        XCTAssertEqual(mockNSBundle.pathForResourceCalledWithFileName, LocalJSONDataStoreFiles.statementDates.rawValue)
    }
    
    func testFetchStatements_withValidJSONOfStatementDates_shouldReturnStatementsAndNilError() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        var receivedStatements : [Statement]?
        var receivedError : Error?
        
        //Act
        sut.fetchStatements { (result: Result<[Statement]>) in
            switch result {
            case .success(let statements):
                receivedStatements = statements
            case .failure(let error):
                receivedError = error
            }
            asynExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 1.0) { error in
            //Assert
            XCTAssertNil(receivedError)
            XCTAssert(receivedStatements?.first?.date == "2014-06-16".date())
            XCTAssert(receivedStatements?.last?.date == "2012-11-16".date())
        }
    }
    
    func testFetchTransactions_withValidJSONOfTransactions_shouldReturnTransactionsAndNilError() {
        //Arrange
        let asynExpectation = expectation(description: "AsynFunction")
        
        var receivedTransactions : [Transaction]?
        var receivedError : Error?
        
        //Act
        sut.fetchTransactions { (result: Result<[Transaction]>) in
            switch result {
            case .success(let transactions):
                receivedTransactions = transactions
            case .failure(let error):
                receivedError = error
            }
            asynExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 2.0) { error in
            //Assert
            XCTAssertNil(receivedError)
            XCTAssert(receivedTransactions?.first?.date == "2014-07-06".date())
            XCTAssert(receivedTransactions?.first?.amount == -0.88)
            XCTAssert(receivedTransactions?.last?.date == "2010-01-15".date())
            XCTAssert(receivedTransactions?.last?.amount == -100)
        }
    }
}
