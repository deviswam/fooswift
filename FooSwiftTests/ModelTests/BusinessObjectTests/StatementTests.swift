//
//  StatementTests.swift
//  FooSwift
//
//  Created by Waheed Malik on 07/06/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import XCTest
@testable import FooSwift

class StatementTests: XCTestCase {
    func testConformanceToStatementProtocol() {
        //Arrange
        let sut = FSStatement(date: Date())
        
        //Assert
        XCTAssertTrue((sut as Any) is Statement, "FSStatement should conforms to Statement protocol")
    }
    
    func testInit_statementShouldBeCreatedWithADate() {
        //Arrange
        let currentDate = Date()
        let sut = FSStatement(date: currentDate)
        
        //Assert
        XCTAssert(sut.date == currentDate, "statement should have a date to get created")
    }
    
    func testStatementShouldHaveAListOfTransactions() {
        //Arrange
        let sut = FSStatement(date: Date())
        sut.transactions = [FSTransaction(date: Date(), amount: -123.34)]
        
        //Assert
        XCTAssertNotNil(sut.transactions)
        XCTAssertEqual(sut.transactions.count, 1)
    }

}
