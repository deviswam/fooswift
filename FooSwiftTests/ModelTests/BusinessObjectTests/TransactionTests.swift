//
//  TransactionTests.swift
//  FooSwift
//
//  Created by Waheed Malik on 07/06/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import XCTest
@testable import FooSwift

class TransactionTests: XCTestCase {

    func testConformanceToTransactionProtocol() {
        //Arrange 
        let sut = FSTransaction(date: Date(), amount: -0.88)
        
        //Assert
        XCTAssertTrue((sut as Any) is Transaction, "FSTransaction should conforms to Transaction protocol")
    }
    
    func testInit_transactionMustBeCreatedWithADateAndAmount() {
        //Arrange
        let currentDate = Date()
        let sut = FSTransaction(date: currentDate, amount: -0.88)
        
        //Assert
        XCTAssert(sut.date == currentDate, "transaction must have a date to get created")
        XCTAssertEqual(sut.amount, -0.88, "transaction must have an amount to get created")
    }
    
    func testTransactionConformsToObjectMapperProtocol() {
        //Arrange
        let sut = FSTransaction(date: Date(), amount: -0.88)
        
        //Assert
        XCTAssertTrue((sut as Any) is ObjectMapper, "transaction should conforms to ObjectMapper protocol")
    }
    
    func testInit_withNoOccurredDateInDictionary_shouldFailInitialization() {
        //Arrange
        let transactionDictionary = ["amount": -0.88]
        let sut = FSTransaction(dictionary: transactionDictionary)
        
        //Assert
        XCTAssertNil(sut, "Transaction should only be created if dictionary has occurred date")
    }
    
    func testInit_withNoAmountInDictionary_shouldFailInitialization() {
        //Arrange
        let transactionDictionary = ["occurred": "2014-07-06"]
        let sut = FSTransaction(dictionary: transactionDictionary)
        
        //Assert
        XCTAssertNil(sut, "Transaction should only be created if dictionary has amount")
    }
    
    func testInit_withOccurredDateAndAmountInDictionary_shouldsetTransactionDateAndAmount() {
        //Arrange
        let transactionDictionary = ["occurred": "2014-07-06", "amount": -0.88] as [String: AnyObject]
        let sut = FSTransaction(dictionary: transactionDictionary)
        
        //Assert
        XCTAssertNotNil(sut, "Transaction should be created with date and amount")
    }
}
